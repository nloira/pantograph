

==============
 Pantograph
==============

-----------------------------------------------------------
 A toolbox for genome-scale metabolic model reconstruction
-----------------------------------------------------------

:Author: Nicolas Loira
:Date: 2016-04


Biopantograph is a collection of python modules and programs for the reconstruction, curation and validation of metabolic models. It reads and writes `SBML <http://sbml.org/Main_Page>`_ files.

Installation
------------

The recommended installation method is::

	pip install biopantograph

Pantograph requires ``libsbml``, but ``pip`` will take care of that requirement. If you don't have permissions to install at the system level, try: ``pip install biopantograph --user``. You can also clone and contribute from the `BitBucket repository <https://bitbucket.org/nloira/pantograph>`_.

Tools
-----

The basic script you need to know is ``pantograph``, which will help you to reconstruct a metabolic model for your target species. ``pantograph`` takes as input an existing SBML model as a template, orthology data between your template and your target genes (from `Inparanoid  <http://inparanoid.sbc.su.se/cgi-bin/index.cgi>`_ and/or `OrthoMCL <http://www.orthomcl.org>`_) and other optional data. Invoke ``pantograph`` as follows::


    $ pantograph -x scaffold.xml -i inparanoid.data -m orthomcl.data -s org1 -t org2 -o out.xml -l out.log

Where ```scaffold.txt``` is an existing, well-curated SBML metabolic model, used as a template for the reconstruction;
``inparanoid.data`` is the output from Inparanoid, comparing the genes from the template and the reconstructed model;
``orthomcl.data`` is the output from OrthoMCL (where ``org1`` is the template model id and ``org2`` is the reconstructed model id);
``out.xml`` is the SBML reconstructed model and ``out.log`` is the reconstruction log. You can reconstruct a model using any of the two orthology methods (Inparanoid or OrthoMCL), although better results are obtained using both.

The ``biopantograph`` package includes many other tools that can be useful for your reconstruction. Try them with the parameter ``--help`` to obtain usage information.

===================== ===============================================================
Tool                  Description
===================== ===============================================================
pantograph            Create a new model, from a template model and orthology files
ptg_addbiggreactions  Add reactions from a list of BiGG ids, to a model
ptg_projector         Create a new model, using an existing one as template
ptg_curateSBML        Add new species/reactions to a model, from a CSV
ptg_inparanoid2rel    Parses Inparanoid output for ptg_projector
ptg_omcl2rel          Parses OrthoMCL output for ptg_projector
===================== ===============================================================

The modules provided can be used to develop your own applications:


===================== =========================================================================================
Module                Description
===================== =========================================================================================
pathtastictools       Algorithms to reconstruct metabolic models from a template
SBMLeditor            High-level modifications to SBML models
BiGG                  Module to handle calls to the `BiGG Web API <http://bigg.ucsd.edu/data_access>`_
BiGG2SBML             Add elements from BiGG API to a SBML file
SBMLutils             High-level utilities
===================== =========================================================================================


Biopantograph has been tested under Linux and OS X. For more information, please visit the official Pantograph website: http://pathtastic.gforge.inria.fr. More documentation coming soon!

Reference
---------

If you use Pantograph for your own reconstructions, please cite:

	Nicolas Loira, Anna Zhukova and David J Sherman. *Pantograph: A template-based method for genome-scale metabolic model reconstruction.* J Bioinform Comput Biol, 13(2): 1550006, 2015.
	DOI: 10.1142/S0219720015500067

For any question, please contact: Nicolas Loira nloira@gmail.com

License
-------

This project is open source and freely available under the `GLP v2 license <http://www.gnu.org/licenses/old-licenses/gpl-2.0.en.html>`_.


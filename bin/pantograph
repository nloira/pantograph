#!/bin/bash

# This file is part of BioPantograph https://bitbucket.org/nloira/pantograph
# Copyright 2009-2016 Nicolas Loira
#
# This library is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Biopantograph.  If not, see <http://www.gnu.org/licenses/>


die () {
    echo >&2 "$@"
    exit 1
}


OUTLOG="out.log"
OUTMODEL="out.xml"

while [[ $# > 1 ]]
do
key="$1"
shift


case $key in
    -r|--rel)
    MORERELDIR="$1"
    shift
    ;;
    -i|--inparanoid)
    INPARANOID="$1"
    shift
    ;;
    -m|--omcl)
    OMCL="$1"
    shift
    ;;
    -s|--source)
    OMCLSOURCE="$1"
    shift
    ;;
    -t|--target)
    OMCLTARGET="$1"
    shift
    ;;
    -x|--template|--scaffold)
    TEMPLATE="$1"
    shift
    ;;
    -o|--out)
    OUTMODEL="$1"
    shift
    ;;    
    -l|--log)
    OUTLOG="$1"
    shift
    ;;
    -f|--force)
    FORCE="-f $1"
    shift
    ;;
    --default)
    DEFAULT=YES
    shift
    ;;
    *)
            # unknown option
    ;;
esac
done

# echo Parameters: $INPARANOID $OMCL $OMCLSOURCE $OMCLTARGET $TEMPLATE $OUTMODEL $OUTLOG

# create temp dir
td=$(mktemp -d /tmp/pantograph.XXXXXX)
# echo Using: $td

# does template exists?
[ -e "$TEMPLATE" ] || die "Template file does not exists: $TEMPLATE"

# does it include the initial xml tag?
line=$(head -n 1 $TEMPLATE)
if [[ $line != \<\?xml* ]]; then
	# echo "error primera linea" $line
	NEWTEMPLATE=$td/newtemplate.xml
	echo '<?xml version="1.0" encoding="UTF-8"?>' > $NEWTEMPLATE
	cat $TEMPLATE >> $NEWTEMPLATE
	sed -i.bak "s/xmlns:=/xmlns=/g" $NEWTEMPLATE
	TEMPLATE=$NEWTEMPLATE
fi


# OrthoMCL

if [[ $OMCL != "" && $OMCL != "None" ]]; then
	[ -e "$OMCL" ] || die "OrthoMCL file does not exists: $OMCL"
	[ -n "$OMCLSOURCE" ] || die "OrthoMCL source not defined"
	[ -n "$OMCLTARGET" ] || die "OrthoMCL target not defined"
	echo Parsing orthomcl: $OMCL $OMCLSOURCE $OMCLTARGET
	ptg_omcl2rel -s $OMCLSOURCE -t $OMCLTARGET $OMCL > $td/omcl.rel
	REL=1
fi

# Inparanoid

if [[ $INPARANOID != "" && $INPARANOID != "None" ]]; then
	[ -e "$INPARANOID" ] || die "Inparanoid file does not exists: $INPARANOID"
	echo Parsing inparanoid: $INPARANOID
	ptg_inparanoid2rel $INPARANOID > $td/inp.rel
	REL=1
fi

# User provided rels


if [[ $MORERELDIR != "" && $MORERELDIR != "None" ]]; then
    [ -d "$MORERELDIR" ] || die "User defined rel directory doesn't exists: $MORERELDIR"
    echo Importing .rel files from: $MORERELDIR
    cp $MORERELDIR/*.rel $td
    REL=1
fi


# Now project
# (we already checked for the existence of the scaffold model)

[ -n "$REL" ] || die "No .rel provided (please use Inparanoid or OrthoMCL)"

# echo projector.py -o $OUTMODEL -l $OUTLOG $TEMPLATE $td/*.rel
ptg_projector $FORCE -o $OUTMODEL -l $OUTLOG $TEMPLATE $td/*.rel

# cleaning
# rm -rf $td

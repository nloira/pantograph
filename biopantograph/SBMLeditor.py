#!/usr/bin/env python
# -*- coding: utf-8 -*-
# This file is part of BioPantograph https://bitbucket.org/nloira/pantograph
# Copyright © 2009-2016 Nicolas Loira
#
# This library is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with BioPantograph.  If not, see <http://www.gnu.org/licenses/>.

"""High level model edit operations/verbs over libsbml"""

from __future__ import absolute_import

import sys
import itertools
from pprint import pprint
from collections import namedtuple
import re

import libsbml

from .SBMLutils import log

MAX_FLOW_BOUND = 1000
ARROWS = ( "<=>", "<=", "=>", "<->", "<-", "->", "NOARROW")

REVERSIBILITY = dict(zip(ARROWS, (True, "left", False, True, "left", False, False)))

SpeciesData = namedtuple('SpeciesData', "id,name,formula,compartment,boundary")
# ReactionData = namedtuple('ReactionData', "id,name,reversibility,reactants,products,geneassociation,genenameassociation,formula")
ReactionData = namedtuple('ReactionData', "id,name,reversibility,geneassociation,formula,enzyme")


__author__ = "Nicolas Loira"
__email__ = "nloira@gmail.com"
__date__ = "14/March/2016"

already_moved = dict()

###################################
# COMMON
###################################

def add_note(element, note):
	if element.isSetNotes():
		element.appendNotes(str(note))
	else:
		element.setNotes(str("<html:body>" + note + "</html:body>"))

def p(text):
	return "<html:p>"+text+"</html:p>"

###################################
# COMPARTMENTS
###################################

def valid_compartment(model, compartment):
	compartments = [c.getId() for c in model.getListOfCompartments()]
	return compartment in compartments


def remove_compartment(model, compartmentid):
	
	# remove all species associated to this compartment
	for species in list(model.getListOfSpecies()):
		if species.getCompartment() == compartmentid:
			model.removeSpecies(species.getId())

	# remove all related reactions
	reactionids = reactions_touching_compartment(model, compartmentid)
	for r in reactionids:
		model.removeReaction(r)

	# now remove the compartment
	model.removeCompartment(compartmentid)


def set_standard_size(model, sizev=1.0):
	assert model

	for c in model.getListOfCompartments():
		if not c.isSetSize():
			c.setSize(sizev)

def create_compartment(model, id, name=None):
	name = id if name is None else name
	newcompartment = model.createCompartment()
	newcompartment.setId(id)
	newcompartment.setName(id)
	return newcompartment

###################################
# SPECIES
###################################

def move_species(model, speciesid, source_comp, target_comp):
	global already_moved

	if speciesid in already_moved:
		return already_moved[speciesid]

	species = model.getSpecies(speciesid)
	assert species.getCompartment() == source_comp

	# if there is an existing species at the target compartment, use that one
	# else, create a new one with the same characteristics

	new_speciesid = speciesid[:-1] + target_comp
	
	if model.getSpecies(new_speciesid) is None:
		new_species = species.clone()
		new_species.setCompartment(target_comp)
		new_species.setId(new_speciesid)
		log(" Cloning %s to %s" % (speciesid, new_species.getId()))
		model.addSpecies(new_species)
		model.removeSpecies(speciesid)
	else:
		log(" Using existing "+new_speciesid)

	already_moved[speciesid] = new_speciesid
	return new_speciesid


def set_formula(species, formula):
	add_note(species, p("FORMULA: " + formula))


def create_species(model, data):
	# data is a SpeciesData or compatible
	if data.id in (None, ""):
		return None

	sid = short2full_speciesid(data.id, data.compartment)

	if model.getSpecies(sid) is not None:
		log("WARNING: refusing to add existing species "+sid)
		return None

	new_species = model.createSpecies()
	new_species.setId(sid)
	new_species.setName(data.name if data.name is not None else data.id)
	new_species.setCompartment(data.compartment)
	set_formula(new_species, data.formula)

	return new_species


def set_species_initial_concentration(model, initial=0.0):
	assert model

	for s in model.getListOfSpecies():
		if not s.isSetInitialConcentration():
			s.setInitialConcentration(initial)


def remove_orphan_species(model):
	removed = []
	for species in list(model.getListOfSpecies()):
		if model.getSpeciesReference(species.getId()) is None:
			removed.append(species.getId())
			species.removeFromParentAndDelete()
	return removed if len(removed) > 0 else None


def short2full_speciesid(id, compartment=None):

	full = id if id.startswith("M_") else "M_"+id
	full = re.sub(r"\[(.*)\]", r"_\1", full)
	full = full.replace("-","_DASH_")
	full = full.replace("(", "_LPAREN_")
	full = full.replace(")", "_RPAREN_")
	full = full.replace("__", "_")

	if compartment is not None and not full.endswith("_"+compartment):
		full = full + "_" + compartment

	return full


###################################
# REACTIONS
###################################

def move_reaction(model, reactionid, source_comp, target_comp):
	"Move species associated to this reaction, from source compartment to target compartment. Create species if necessary"
	assert model is not None

	if not valid_compartment(model, source_comp) or not valid_compartment(model, target_comp):
		log("MOVE: invalid compartments (%s,%s)" % (source_comp, target_comp))
		return False

	reaction = model.getReaction(reactionid)
	speciesReferences = itertools.chain(reaction.getListOfProducts(), reaction.getListOfReactants())
	
	for sr in speciesReferences:
		sid = sr.getSpecies()
		species = model.getSpecies(sid)
		if species is None:
			sr.setSpecies(already_moved[sid])  # probably already moved
		elif species.getCompartment() == source_comp:
			log("Moving %s from [%s] to [%s]" % (species.getId(), source_comp, target_comp))
			new_speciesid = move_species(model, sid, source_comp, target_comp)
			sr.setSpecies(new_speciesid)

	return True


def reactions_touching_compartment(model, compartmentid):

	csufix = "_"+compartmentid
	touching = []

	for reaction in model.getListOfReactions():
		speciesReferences = itertools.chain(reaction.getListOfProducts(), reaction.getListOfReactants())
		inCompartment = [s for s in speciesReferences if s.getSpecies().endswith(csufix)]
		if len(inCompartment) > 0:
			touching.append(reaction.getId())

	return touching

def remove_requirement(model, reactionid, speciesid):
	reaction = model.getReaction(reactionid)
	reaction.removeReactant(speciesid)

def add_requirement(model, reactionid, speciesid, stoichiometry):
	reaction = model.getReaction(reactionid)
	sref = reaction.createReactant()
	sref.setSpecies(speciesid)
	sref.setStoichiometry(stoichiometry)


def set_gene_association(model, reactionid, gene_association, clean=True):
	reaction = model.getReaction(reactionid)

	return _set_gene_association(reaction, gene_association, clean)


def _set_gene_association(reaction, gene_association, clean=True):

	if clean: remove_gene_association(reaction)
	add_note(reaction, p("GENE_ASSOCIATION: " + gene_association))
	return reaction



def remove_gene_association(reaction):
	if reaction.isSetNotes() is False:
		return
	lines = reaction.getNotesString().split("\n")
	filterGA = [l for l in lines if l.find("GENE_ASSOCIATION") == -1]
	reaction.setNotes("\n".join(filterGA))


def set_reversibility(model, reactionid, reversible=True):
	reaction = model.getReaction(reactionid)
	if reaction is None:
		log("Unable to set reversibility to:"+reactionid)
		return

	return _set_reversibility(reaction, reversible)


def _set_reversibility(reaction, reversible=True):

	reaction.setReversible(reversible==True)

	kl = reaction.getKineticLaw() if reaction.isSetKineticLaw() else create_kinetics(reaction)
	kl.getParameter('LOWER_BOUND').setValue(-MAX_FLOW_BOUND if reversible is not False else 0)
	kl.getParameter('UPPER_BOUND').setValue(MAX_FLOW_BOUND if reversible is not "left" else 0)


def create_kinetics(reaction, lb=-MAX_FLOW_BOUND, ub=MAX_FLOW_BOUND):
	kl = reaction.createKineticLaw()
	add_parameter(kl, 'LOWER_BOUND', lb)
	add_parameter(kl, 'UPPER_BOUND', ub)
	add_parameter(kl, 'OBJECTIVE_COEFFICIENT', 0.0)
	add_parameter(kl, 'FLUX_VALUE', 0.0)
	kl.setFormula(" FLUX_VALUE ")
	return kl

def add_parameter(kl, id, value):
	p = kl.createParameter()
	p.setId(id)
	p.setValue(value)
	p.setUnits("mmol_per_gDW_per_hr")
	return p


def parse_stoich_species(species):
	for s in species.split(" + "):
		elems = s.strip().split(" ")
		stoich = elems[0] if len(elems) > 1 else 1
		speciesid = elems[-1]
		yield (int(stoich), speciesid)


def parse_formula(f):
	for a in ARROWS:
		if a in f: break

	if a == "NOARROW":
		print "Invalid formula: "+f
		return None

	reactants, products = f.split(a)

	ssreactants = [ss for ss in parse_stoich_species(reactants)]
	ssproducts = [ss for ss in parse_stoich_species(products)]

	return (a, ssreactants, ssproducts)


def add_species_reference(model, factory, stoichiometry, speciesid):
	if model.getSpecies(speciesid) is None:
		log("WARNING: reaction references non-existing species: "+speciesid)
		return

	sref = factory()
	sref.setSpecies(speciesid)
	sref.setStoichiometry(stoichiometry)


def add_formula(model, reaction, formula):
	full = short2full_speciesid

	arrow, ssreactants, ssproducts = parse_formula(formula)
	_set_reversibility(reaction, REVERSIBILITY[arrow])

	map(lambda x: add_species_reference(model, reaction.createReactant, x[0], full(x[1])), ssreactants)
	map(lambda x: add_species_reference(model, reaction.createProduct, x[0], full(x[1])), ssproducts)


def create_reaction(model, data):
	# data is a ReactionData or compatible
	if data.id in (None, ""):
		return None

	rid = data.id if data.id.startswith("R_") else "R_"+data.id

	if (model.getReaction(data.id) is not None) or (model.getReaction(rid) is not None):
		log("WARNING: refusing to add existing reaction "+data.id)
		return None

	new_reaction = model.createReaction()
	new_reaction.setId(rid)
	new_reaction.setName(data.name if data.name is not None else data.id)
	new_reaction.setReversible(data.reversibility)

	if data.formula is not None:
		add_formula(model, new_reaction, data.formula)

	if data.geneassociation is not None and data.geneassociation is not "":
		_set_gene_association(new_reaction, data.geneassociation, False)

	if data.enzyme is not None and data.enzyme is not "":
		add_note(new_reaction, p("EC: "+data.enzyme))


	return new_reaction
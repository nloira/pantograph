#!/usr/bin/env python
# -*- coding: utf-8 -*-
# This file is part of BioPantograph https://bitbucket.org/nloira/pantograph
# Copyright © 2016 Nicolas Loira
#
# This library is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with BioPantograph.  If not, see <http://www.gnu.org/licenses/>.

"""miniSBtab: parses CSV files in SBtab format (http://www.sbtab.net).
It supports a very limited selection of data. It's a solution while the
SBtab python libraries are not pip installable."""


from __future__ import print_function

import csv
import logging
import os.path
import sys
#import exceptions

from . import SBMLeditor as SE

class FormatError(Exception):
	def __init__(self, value):
		self.value = value
		logging.error(value)
	def __str__(self):
		return repr(self.value)


def parse_sbtab(csvfile, SBtabtype):
	if not os.path.isfile(csvfile):
		raise exceptions.IOError("%s file %s do not exists" % (SBtabtype.capitalize(), csvfile))

	fd = open(csvfile, "r")

	header1 = fd.readline()
	if not header1.startswith("!!SBtab"):
		raise FormatError("%s file %s in not in SBtab format" % (SBtabtype.capitalize(), csvfile))

	header2 = fd.readline()
	if not header2.startswith("!"):
		raise FormatError("%s file %s in not in SBtab format" % (SBtabtype.capitalize(), csvfile))


	headers = [x[1:] for x in header2[:-1].split("\t")]

	reader = csv.DictReader(fd, fieldnames=headers, delimiter="\t")

	for row in reader:
		if row[headers[0]].startswith("#"): continue
		yield row


def parse_compounds(compounds_file):
	for c in parse_sbtab(compounds_file, "compounds"):
		yield SE.SpeciesData(c['ID'], c['Name'], c['StructureFormula'], c['Location'].translate(None,"[]"), False)


def parse_reactions(reactions_file):
	for r in parse_sbtab(reactions_file, "reactions"):
		yield SE.ReactionData(r['ID'], r['Name'], r['IsReversible'].upper()=="TRUE", r['GeneAssociation'], r['ReactionFormula'], r.get('Enzyme', None))


if __name__ == '__main__':
	for species in parse_compounds(sys.argv[1]):
		print(species)

	for reactions in parse_reactions(sys.argv[2]):
		print(reactions)




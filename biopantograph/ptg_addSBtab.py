#!/usr/bin/env python
# -*- coding: utf-8 -*-
# This file is part of BioPantograph https://bitbucket.org/nloira/pantograph
# Copyright © 2009-2016 Nicolas Loira
#
# This library is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with BioPantograph.  If not, see <http://www.gnu.org/licenses/>.

"""ptg_addSBtab: add reactions and species to an existing sbml file,
making sure that the metabolites referenced by reactions are also present!"""

from __future__ import print_function

import argparse
import libsbml
import csv

from . import SBMLutils as SU, SBMLeditor as SE
from . import miniSBtab

__author__ = "Nicolas Loira"
__email__ = "nloira@gmail.com"
__date__ = "12/May/2016"


def main():
	"""Add BiGG reactions to an existing SBML model"""
	# args
	parser = argparse.ArgumentParser(description="add reactions from a SBtab file")
	parser.add_argument("-s", "--sbml", required=True, help="SBML model where reactions will be added")
	parser.add_argument("-m", "--metabolites", help="SBtab file with new metabolites")
	parser.add_argument("-r", "--reactions", required=True, help="SBtab file with new reactions")
	parser.add_argument("-o", "--output", required=True, help="output SBML file")
	parser.add_argument("-a", "--autocompartments", action="store_true", help="create compartments if needed")
	args = parser.parse_args()

	# SBML
	sbml = SU.read_sbml(args.sbml)
	model = sbml.getModel()

	# CSVs
	reactions = miniSBtab.parse_reactions(args.reactions)
	metabolites = miniSBtab.parse_compounds(args.metabolites) if args.metabolites else None	

	# add elements
	if metabolites:
		for m in metabolites:
			if not SE.valid_compartment(model, m.compartment):
				if args.autocompartments:
					SE.create_compartment(model, m.compartment)
				else:
					SU.log("Error: Invalid compartment: %s. Skipping %s." % (m.compartment, m.id))
					continue
			SE.create_species(model, m)

	for r in reactions:
		SE.create_reaction(model,r)

	# close
	libsbml.writeSBMLToFile(sbml, args.output)



if __name__ == '__main__':
	main()
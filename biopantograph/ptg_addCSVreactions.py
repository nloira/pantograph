#!/usr/bin/env python
# -*- coding: utf-8 -*-
# This file is part of BioPantograph https://bitbucket.org/nloira/pantograph
# Copyright © 2009-2016 Nicolas Loira
#
# This library is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with BioPantograph.  If not, see <http://www.gnu.org/licenses/>.

"""ptg_addCSVreactions: add reactions and species to an existing sbml file,
making sure that the metabolites referenced by reactions are also present!"""

from __future__ import print_function

import argparse
import libsbml
import csv

from . import SBMLutils as SU, SBMLeditor as SE


__author__ = "Nicolas Loira"
__email__ = "nloira@gmail.com"
__date__ = "22/April/2016"


def parse_reactions(reactions_csv):
	allReactions=[]
	with open(reactions_csv, "rb") as reactions_fd:
		for line in csv.reader(reactions_fd):
			if line[0] in ("", "ID"): continue
			if line[0].startswith('#'): continue
			allReactions.append(SE.ReactionData(id=line[0], name=line[1], reversibility=line[2], 
				reactants=line[3].split(';'), products=line[4].split(';'), geneassociation=line[5],
				genenameassociation=line[6], formula=line[7]))

	return allReactions

def parse_metabolites(species_csv):
	allSpecies=[]
	with open(species_csv, "rb") as species_fd:
		for line in csv.reader(species_fd):
			if line[0] in ("", "ID"): continue
			if line[0].startswith('#'): continue
			allSpecies.append(SE.SpeciesData(id=line[0], name=line[1], formula=line[2],
				compartment=line[3], boundary=False))

	return allSpecies


def main():
	"""Add BiGG reactions to an existing SBML model"""
	# args
	parser = argparse.ArgumentParser(description="add reactions from a table, using BiGG 2.0 API")
	parser.add_argument("-s", "--sbml", required=True, help="SBML model where reactions will be added")
	parser.add_argument("-m", "--metabolites", help="CSV file with new metabolites")
	parser.add_argument("-r", "--reactions", required=True, help="CSV file with new reactions")
	parser.add_argument("-o", "--output", required=True, help="output SBML file")
	parser.add_argument("-a", "--autocompartments", action="store_true", help="create compartments if needed")
	args = parser.parse_args()

	# SBML
	sbml = SU.read_sbml(args.sbml)
	model = sbml.getModel()

	# CSVs
	reactions = parse_reactions(args.reactions)
	metabolites = parse_metabolites(args.metabolites) if args.metabolites else None	

	# add elements

	if metabolites:
		for m in metabolites:
			if not SE.valid_compartment(model, m.compartment):
				if args.autocompartments:
					SE.create_compartment(model, m.compartment)
				else:
					SU.log("Error: Invalid compartment: %s. Skipping %s." % (m.compartment, m.id))
					continue
			SE.create_species(model, m)
	for r in reactions:
		print(r.id)


	# close
	libsbml.writeSBMLToFile(sbml, args.output)



if __name__ == '__main__':
	main()